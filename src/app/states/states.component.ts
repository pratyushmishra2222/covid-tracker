import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { model } from '../model';
import { Title } from '@angular/platform-browser';
@Component({
  selector: 'app-states',
  templateUrl: './states.component.html',
  styleUrls: ['./states.component.css']
})
export class StatesComponent implements OnInit {
  
  hello : string = "";
  constructor(private service : ApiService,private TitleService:Title) { }
  dataList : Array<model> = [];
  val:boolean=false;
  ngOnInit(): void {
    this.TitleService.setTitle("states Page")
    this.val=true;
    this.service.getStatesData().subscribe((val:any)=>{
    this.val=false;
     val.statewise.forEach((element:any) => {
      let obj = {
        state : element.state.toString().slice(0,19),
        active : element.active,
        confirmed : element.confirmed,
        deaths : element.deaths,
        recovered : element.recovered
      } 
      console.log(obj);
      
      if(obj.state !== "State Unassigned" && obj.state !== "Total")
        this.dataList.push(obj);
        
     });
      
     
    })
  }
}
