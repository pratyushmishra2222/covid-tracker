import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CountriesComponent } from './countries/countries.component';
import { NavbarComponent } from './navbar/navbar.component';
import { StatesComponent } from './states/states.component';



const routes: Routes = [
  {path:'',redirectTo:'home',pathMatch:'full'},
  {path:'home',component:NavbarComponent},

  {path:'home',children:[
    {path:'states',component:StatesComponent},
    {path:'countries',component:CountriesComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
