import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpclient : HttpClient) { }
  states : string = "https://data.covid19india.org/data.json";
  countries : string = "https://api.covid19api.com/summary";
  
  getStatesData(){
    return this.httpclient.get(this.states);
  }

  getCountriesData(){
    return this.httpclient.get(this.countries);
  }

}
