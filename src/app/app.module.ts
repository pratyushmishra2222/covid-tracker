import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TableComponent } from './table/table.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SpinnersAngularModule } from 'spinners-angular';
import { NavbarComponent } from './navbar/navbar.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { StatesComponent } from './states/states.component';
import { CountriesComponent } from './countries/countries.component';
import { DetailsComponent } from './details/details.component';
import { PipePipe } from './pipe.pipe';
import { BackComponent } from './back/back.component';
import { HomePageComponent } from './home-page/home-page.component';
import { TableFilterPipe } from './table-filter.pipe';
@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
   NavbarComponent,
   StatesComponent,
    CountriesComponent,
    DetailsComponent,
    PipePipe,
    BackComponent,
    HomePageComponent,
    TableFilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    SpinnersAngularModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
