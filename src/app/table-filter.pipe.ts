import { Pipe, PipeTransform } from '@angular/core';
import { type } from './countries/countries.component';
import { model } from './model';

@Pipe({
  name: 'tableFilter'
})
export class TableFilterPipe implements PipeTransform {

  transform(dataList: any[], searchText: string): any[] {  
    
    if(!searchText || !dataList){
      return dataList;
    }
    
    console.log();
    let newDataList  = []
    if(Object.keys(dataList[0]).includes("state")){
      newDataList =  dataList.filter((model:model) => model.state.toString().toLowerCase().startsWith(searchText.toString().toLowerCase()))
    }
    else{
      newDataList =  dataList.filter((type:type) => type.Country.toString().toLowerCase().startsWith(searchText.toString().toLowerCase()))
    }
   
    return (
      newDataList
    )
  } 
  

}
