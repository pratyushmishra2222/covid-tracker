import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit 
{

  @Input() list : any;
  @Input() items : any[] = [];
  @Input() value : string="";

  onChangePage(list : any) {
    // update current page of items
    this.list = list;
}
  ngOnInit() {
    // an example array of 150 items to be paged
    //this.items = Array(150).fill(0).map((x, i) => ({ id: (i + 1), name: `Item ${i + 1}`}));
}

  getHeaders(){
    let headers : string[]=[];
    if(this.list){
      this.list.forEach((value : {}) =>{
        Object.keys(value).forEach((key)=>{
          if(!headers.find((header)=>header===key)){
            headers.push(key)
          }
        })
      })
    }
    return headers;
  }
  goToTheLink(e : any){
    
      window.open("https://www.google.com/search?q="+e+" covid cases", "_blank");
    
  }
  call(){
    window.scrollTo(0,0);
  }
  p:number=1;
}

 

