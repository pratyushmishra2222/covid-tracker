import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

import { Title } from '@angular/platform-browser';
export interface type{
  Country : string,
  TotalConfirmed : number,
  TotalDeaths : number,
  TotalRecovered : number
}

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})

export class CountriesComponent implements OnInit {

  
  hello : string = "";
 
  constructor(private service : ApiService,private titleservice : Title) { }
  dataList : type[] = [];
  val : boolean = false;
  ngOnInit(): void {
    this.titleservice.setTitle("countries page")
    this.val = true;
   this.service.getCountriesData().subscribe((val:any)=>{
      this.val=false;
      val.Countries.forEach((element:any) => 
      {
        let obj = {
          Country : element.Country.toString(),
          TotalConfirmed : element.TotalConfirmed,
          TotalDeaths : element.TotalDeaths,
          TotalRecovered : element.TotalConfirmed-element.TotalDeaths
        } 
        
        this.dataList.push(obj);
      });
        
      
      })
  }
 

}
